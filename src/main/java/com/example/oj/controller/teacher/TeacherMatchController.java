package com.example.oj.controller.teacher;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.SubjectLists;

import com.example.oj.entity.Resp;
import com.example.oj.entity.dao.Match;
import com.example.oj.entity.teamSubject.TeamSubjectInfo;
import com.example.oj.service.MatchService;
import com.example.oj.service.SubjectService;
import com.example.oj.tool.MyHttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/2/20 13:22
 */
@RestController
@RequestMapping("/teacher")
public class TeacherMatchController {

    private static final int maxNUm=6; //设置单页最大显示数字

    private final MatchService matchService;
    private final SubjectService subjectService;


    @Autowired
    TeacherMatchController(MatchService matchService,SubjectService subjectService) {
        this.matchService = matchService;
        this.subjectService=subjectService;
    }

    /**
     * 发布比赛
     */
    @PostMapping("/releaseMatch")
    public Resp releaseMatch(@RequestParam("name")String name,
                             @RequestParam("teacher_name") String teacher_name,
                             @RequestParam("describes") String describe,
                             @RequestParam("maxNumber") int maxNumber,
                             @RequestParam("signStart") String signStart,
                             @RequestParam("signEnd") String signEnd,
                             @RequestParam("startTime") String startTime,
                             @RequestParam("endTime") String endTime){
        Match match = new Match();
        match.setName(name);
        match.setTeacherName(teacher_name);
        match.setDescribes(describe);
        match.setNumberMan(maxNumber);
        match.setIsDelete(0);
        match.setSignStart(signStart);
        match.setSignEnd(signEnd);
        match.setStartTime(startTime);
        match.setEndTime(endTime);
        Long millis = System.currentTimeMillis();
        match.setMId(millis);
        boolean b = matchService.releaseMatch(match);
        if(b){
            return new Resp(MyHttpStatus.SUCCESS,"表单已提交，请等待或联系管理员审核",match);
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"提交失败");
    }

    /**
     * 上传比赛文件
     * @param request
     * @param file
     * @return
     */
    @PostMapping("UploadMatchFile")
    public Resp UploadMatchFile(HttpServletRequest request, @RequestPart(value = "file",required = false) MultipartFile file){
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        String matchName = multipartRequest.getParameter("matchId");
        boolean b = matchService.UploadMatchFile(file, matchName);
        if(b){
            return new Resp(MyHttpStatus.SUCCESS,"文件上传成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"上传失败");
    }
    /**
     * 查询比赛
     */
    @PostMapping("findMatch")
    public Resp findMatch(@RequestParam(value = "status",defaultValue = "0") int i,
                          @RequestParam(value = "matchName",required = false) String matchName,
                          @RequestParam(value = "page",defaultValue = "1") int page,
                          @RequestParam(value = "pageSize",defaultValue = "10") int pageSize){

        IPage<Match> match = matchService.findMatchPage(i, matchName, page, pageSize);
        if(match != null&&match.getRecords().size() > 0){
            Map<String,Object> map = new HashMap<>();
            map.put("match",match.getRecords());
            map.put("total",match.getTotal());
            return new Resp(MyHttpStatus.SUCCESS, map);
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"没有比赛");
    }


    /**
     * 排行榜--->老师端
     * 通过个人刷题数量/团队刷题数量来排行
     * @Author dovesess
     */


    @PostMapping("/listToTeacher")
    public Resp listToTeacher( @RequestParam("page") Integer page,@RequestParam("number") Integer number){
        //int maxNUm=6;


        if (page<=0||number<=0){
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"玩负数，一边玩去");
        }

        //查询个人的刷题数量
        Map<String, Object> map = subjectService.listCheck(page, number);
        if (map.get("datas")!=null){
                return new Resp(MyHttpStatus.SUCCESS,"查询成功",map);
            }else {
                return new Resp(MyHttpStatus.BUSINESS_ERROR,"查询失败");
            }
    }

    /**
     * 排行榜--->老师端
     * /团队刷题数量来排行
     *
     * @Author dovesess
     */
    @PostMapping("/listToTeacherByTeam")
    public Resp listToTeacherByTeam( @RequestParam("page") Integer page,@RequestParam("number") Integer number){
        //int maxNUm=6;


        if (page<=0 || number<=0){
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"玩负数，一边玩去");
        }


        Map<String, Object> map = subjectService.listCheckTeam(page, number);

        if (map.get("datas")==null){
            return new Resp(MyHttpStatus.SUCCESS,"查询为空");
        }else {
            return new Resp(MyHttpStatus.SUCCESS,"查询成功",map);
        }

    }
}























