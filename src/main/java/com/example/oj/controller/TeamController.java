package com.example.oj.controller;

import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.TeamUser;
import com.example.oj.entity.dao.Team;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.TeamVo;
import com.example.oj.mapper.connectMapper.TeamUserMapper;
import com.example.oj.service.TeamService;
import com.example.oj.service.UserService;
import com.example.oj.service.impl.TeamServiceImpl;
import com.example.oj.tool.MyHttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2021/12/31 18:35
 */
@RestController
@RequestMapping("/team")
public class TeamController {

    private final TeamService teamService;
    private final UserService userService;
    private final TeamUserMapper teamUserMapper;
    @Autowired
    public TeamController(TeamService teamService,
                          UserService userService,
                          TeamUserMapper teamUserMapper){
        this.teamService=teamService;
        this.userService=userService;
        this.teamUserMapper=teamUserMapper;
    }

    /**
     * 创建队伍
     * @param teamVo
     * @return
     */
    @PostMapping("/createTeam")
    public Resp createTeam(TeamVo teamVo){
        boolean result = teamService.creatTeam(teamVo);
        Team team = teamService.findAll(teamVo.getName());
        Map<String,Object> map = new HashMap();
        map.put("teamId",team.getId());
        map.put("name",team.getName());
        map.put("captionId",team.getCaptionId());
        map.put("describe",team.getDescribes());
        map.put("isCaptain",0);
        map.put("hasGroup",0);
        map.put("instructor_name",team.getInstructor_name());
        if(result){

            return new Resp(MyHttpStatus.SUCCESS,"创建成功", map);
        }
        else return new Resp(MyHttpStatus.BUSINESS_ERROR,"创建失败");
    }

    /**
     * 解散队伍
     * @param captionId
     * @return
     */
    @PostMapping("/deleteTeam")
    public Resp deleteTeam(@RequestParam("captionId") Integer captionId){
        boolean result = teamService.deleteTeam(captionId);

        Map<String,Object> map=new HashMap<>();
        //判断是否为队长
        if(userService.findTeam(captionId)==null) map.put("isCaptain",1);
        else map.put("isCaptain",0);
        //判断是否有队伍
        if(teamUserMapper.findByUserID(captionId)!=null) map.put("hasGroup",0);
        else map.put("hasGroup",1);

        if(result) return new Resp(MyHttpStatus.SUCCESS,"删除成功",map);
        else return new Resp(MyHttpStatus.BUSINESS_ERROR,"删除失败",map);
    }


    /**
     *添加成员
     */
    @PostMapping("/addMember")
    public Resp addMember(@RequestParam("memberId") String invitationCode,@RequestParam("teamId") Integer teamId){

        if (teamService.addMember(invitationCode,teamId)) {
            return new Resp<>(MyHttpStatus.SUCCESS,"添加成功");
        }else {
            return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"操作失败");
        }

    }

    @PostMapping("/deleteMember")
    public Resp deleteMember(@RequestParam("userId") Integer UserId,@RequestParam("teamId") Integer teamId){
        Integer integer  = teamService.deleteMember(UserId, teamId);

        if (integer==1){
            return new Resp(MyHttpStatus.SUCCESS,"删除成功");
        }else {
            return new Resp(MyHttpStatus.USER_ACCOUNT_ERROR,"操作失败");
        }

    }

    @PostMapping("/findMember")
    public Resp findMember(@RequestParam("teamId") Integer teamId){

        List<User> members = teamService.findMember(teamId);
        if (members!=null && members.size()!=0){
            return new Resp(MyHttpStatus.SUCCESS,"查询成功",members);
        }else {
            return new Resp(MyHttpStatus.USER_LOGIN_ERROR,"查询失败");
        }
    }

    @PostMapping ("/findAllInfo")
    public Resp findAllInfo(@RequestParam("userId") int userId){
        Team team = teamService.findAll(userId);
        Map<String,Object> map = new HashMap();
        if(team==null){
            //判断是否为队长
            if(userService.findTeam(userId)==null) map.put("isCaptain",1);
            else map.put("isCaptain",0);
            //判断是否有队伍
            if(teamUserMapper.findByUserID(userId)!=null) map.put("hasGroup",0);
            else map.put("hasGroup",1);
            return new Resp(MyHttpStatus.SUCCESS,map);
        }

        map.put("teamId",team.getId());
        map.put("name",team.getName());
        map.put("captionId",team.getCaptionId());
        map.put("describe",team.getDescribes());

        map.put("instructor_name",team.getInstructor_name());

        //判断是否为队长
        if(userService.findTeam(userId)==null) map.put("isCaptain",1);
        else map.put("isCaptain",0);
        //判断是否有队伍
        if(teamUserMapper.findByUserID(userId)!=null) map.put("hasGroup",0);
        else map.put("hasGroup",1);
        return new Resp(MyHttpStatus.SUCCESS,map);
    }
}
