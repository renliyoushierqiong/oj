package com.example.oj.controller;

import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.MatchUserTeam;
import com.example.oj.entity.dao.Match;
import com.example.oj.entity.dao.Team;
import com.example.oj.mapper.TeamMapper;
import com.example.oj.mapper.connectMapper.MatchUserTeamMapper;
import com.example.oj.service.MatchService;
import com.example.oj.service.impl.MatchServiceImpl;
import com.example.oj.tool.FileUpload;
import com.example.oj.tool.MyHttpStatus;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 比赛相关业务
 * @author 良良
 * @date 2021/12/31 18:14
 */
@Slf4j
@RestController
@RequestMapping("/match")
public class MatchController {
    private  final MatchService matchService;

    @Autowired
    public MatchController(MatchService matchService){
        this.matchService=matchService;
    }

    /**
     * 请求比赛ID
     * @return
     *
     */

    @Value("${files.matches.results}")
    private String resultPath;//成绩文件夹存放的地方

    @GetMapping("/findMatch")
    public Resp findMatch(){
        List<Match> match = matchService.findMatch(2);

        if(!match.isEmpty())return new Resp(MyHttpStatus.SUCCESS,"查询成功",match);
        else{
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"查询失败");
        }
    }

    /**
     * 报名比赛
     * @param teamId
     * @param matchId
     * @return*/
//TODO　测试和上方findMatch报一种类型错误
    @PostMapping("/applyMatch")
    public Resp applyMatch(@RequestParam("teamId") Integer teamId,
                           @RequestParam("matchId") Integer matchId ){
        boolean result = matchService.applyMatch(teamId, matchId);

        if(result)return new Resp(MyHttpStatus.SUCCESS,"报名成功");
        else{
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"报名失败");
        }
    }
    /**
     * 比赛文件下载
     */
    @PostMapping("/downloadFile")

    public Resp downloadFile(@RequestParam("matchId") String matchName, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String fileName = matchService.findFile(matchName);

        if(fileName!=null){

            File file = new File("match/",fileName);
            log.info("文件路径：   {}",file.getAbsolutePath());
            if (file.exists()) {
                response.reset();
                response.setContentType("application/octet-stream;charset=utf-8");
                response.addHeader("Content-Disposition", "attachment;fileName=" + fileName +";filename*=utf-8''"+ URLEncoder.encode(fileName,"utf-8"));
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally { // 做关闭操作
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"下载失败");
    }

    //获取历史比赛成绩 （问题是比完赛之后才有这数据）
    @GetMapping("/scoresOfHistiorMatch")
    public Resp scoresOfHistiorMatch(@RequestParam("userId") Integer userid){

        return null;
    }

    /**
     * 获取成绩文件
     * 某场比赛id
     * @param response
     */

    @PostMapping("/getResultsFile")
    public void getFileTest( @RequestParam("resultStatus") String resultStatus,HttpServletResponse response){

        //先通过id获取---->文件name

        String fileName=matchService.getFileNameByResultStatus(resultStatus);
        try {
            FileUpload.downloadFile(resultPath,fileName,response);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }



}
