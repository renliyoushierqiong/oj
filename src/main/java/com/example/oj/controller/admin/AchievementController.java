package com.example.oj.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.UserAchievement;
import com.example.oj.entity.dao.Achievement;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.AchievemrntVo;
import com.example.oj.mapper.AchievementMapper;
import com.example.oj.mapper.UserMapper;
import com.example.oj.mapper.connectMapper.UserAchievementMapper;
import com.example.oj.tool.MyHttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 良良
 * @date 2022/2/18 0:04
 * 管理员对于成就的添加删除修改
 */
@RestController
@RequestMapping("/admin")
public class AchievementController {

    private final AchievementMapper achievementMapper;

    private  final UserMapper userMapper;


    @Autowired
    AchievementController(AchievementMapper achievementMapper , UserMapper userMapper) {
        this.achievementMapper = achievementMapper;
        this.userMapper = userMapper;
    }


    /**
     * 查看成就
     */
    @GetMapping("getAchievement")
    public Resp getAchievement(){
//        List<Achievement> all;
//        try {
//            all = achievementMapper.findAll();
//        }catch (Exception e) {
//            return new Resp(MyHttpStatus.SUCCESS,"没有成就");
//        }
//
//        return new Resp(MyHttpStatus.SUCCESS, all);

        List<User> users = userMapper.selectList(null);

        List<AchievemrntVo> result = new ArrayList<>();

        for (User user : users) {
            AchievemrntVo achievemrntVo = new AchievemrntVo();
            achievemrntVo.setName(user.getName());
            achievemrntVo.setSno(user.getSno());
            achievemrntVo.setId(user.getId());

            QueryWrapper<Achievement> qw = new QueryWrapper<>();
            qw.eq("user_id" , user.getId());
            List<Achievement> achievements = achievementMapper.selectList(qw);

            achievemrntVo.setAchievemrntVoList(achievements);

            if(user.getId()==1){
                continue;
            }
            result.add(achievemrntVo);

        }

        if(result.isEmpty()){
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"没有成就");
        }else{
            return new Resp(MyHttpStatus.SUCCESS,"查询成功",result);
        }

    }
    /**
     * 修改成就
     */
    @PostMapping("updateAchievement")
    public Resp updateAchievement( Achievement achievement){
        int i = achievementMapper.updateById(achievement);
        if(i==1){
            return new Resp(MyHttpStatus.SUCCESS, "修改成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR, "修改失败");

    }

    /**
     * 绑定成就
     */
    @PostMapping("bindingAchievement")
    public Resp bindingAchievement( @RequestParam("name") String name,
                                    @RequestParam("types") int types,
                                    @RequestParam("sno") int sno){
        int userID;
        try {
            userID = achievementMapper.findBySno(sno);
        }catch (Exception e) {
            return new Resp(MyHttpStatus.BUSINESS_ERROR, "学号为空");
        }
        Achievement achievement = new Achievement();
        achievement.setName(name);
        achievement.setTypes(types);
        achievement.setUserId(userID);
        if(achievementMapper.insert(achievement)==1){
            return new Resp(MyHttpStatus.SUCCESS, "绑定成就成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR, "绑定失败");


    }

    @PostMapping("deleteAchievement")
    public Resp deleteAchievement(@RequestParam("id") String id ){
        int result = achievementMapper.deleteById(id);
        if(result==1){
            return new Resp(MyHttpStatus.SUCCESS, "删除成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR, "删除失败");
    }

}
