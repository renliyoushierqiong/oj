package com.example.oj.controller.admin;

import com.example.oj.entity.Resp;
import com.example.oj.entity.dao.Rotation;
import com.example.oj.mapper.ManagerMapper;
import com.example.oj.mapper.RotationMapper;
import com.example.oj.mapper.connectMapper.TeamUserMapper;
import com.example.oj.service.UserService;
import com.example.oj.tool.EmailCode;
import com.example.oj.tool.FileUpload;
import com.example.oj.tool.FileUtil;
import com.example.oj.tool.MyHttpStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/27 22:38
 */
@RestController
@RequestMapping("/admin")
@Slf4j
public class PhotoController {


    private final RotationMapper rotationMapper;
    @Autowired
    public PhotoController(RotationMapper rotationMapper){
        this.rotationMapper = rotationMapper;
    }

    @GetMapping("/Rotation")
    public Resp RotationMap(HttpServletRequest request){
        List<Rotation> rotations = rotationMapper.findAll();
        if(rotations!=null){
            return new Resp(MyHttpStatus.SUCCESS,rotations);
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"");
    }


    @PostMapping("/updateRotation")
    public Resp updateRotation(@RequestPart("file") MultipartFile file,HttpServletRequest request) throws IOException {
        String path = "/OJ/img";
        String nowFileName = file.getOriginalFilename();

        File fileParent = new File(path);
        if (!fileParent.exists()) {
            fileParent.mkdirs();
        }
        File f = new File(path+"/",nowFileName);
        if(!f.exists()){
            f.createNewFile();
        }
        file.transferTo(f);
        rotationMapper.insert(new Rotation(nowFileName));

        return new Resp(MyHttpStatus.SUCCESS,"上传成功");
    }

    @PostMapping("/deleteRotation")
    public Resp deleteRotation(@RequestParam("fileName") String fileName,HttpServletRequest request) throws FileNotFoundException {

        String path = "/OJ/img";

        File f = new File(path+"/", fileName);

        boolean delete = f.delete();
        if (delete) {
            rotationMapper.delete(fileName);
            return new Resp(MyHttpStatus.SUCCESS,"删除成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR,"删除失败");
    }



}
