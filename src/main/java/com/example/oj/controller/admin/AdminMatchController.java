package com.example.oj.controller.admin;

import com.example.oj.entity.Resp;
import com.example.oj.entity.dao.Match;
import com.example.oj.mapper.MatchMapper;
import com.example.oj.service.MatchService;
import com.example.oj.tool.FileRight;
import com.example.oj.tool.FileUpload;
import com.example.oj.tool.MyHttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author 良良
 * @date 2022/2/20 15:22
 */
@RestController
@RequestMapping("/admin")
public class AdminMatchController {

    @Value("${files.matches.results}")
    private String resultPath;

    private final MatchService matchService;
    private final MatchMapper matchMapper;
    @Autowired
    AdminMatchController(MatchService matchService,MatchMapper matchMapper) {
        this.matchService = matchService;
        this.matchMapper= matchMapper;
    }

    /**
     * 审核修改比赛
     */
    @PostMapping("/judgeMatch")
    public Resp judgeMatch(Match match){
        boolean b = matchService.judgeMatch(match);
        if(b){
            return new Resp(MyHttpStatus.SUCCESS,"操作成功");
        }
        return new Resp(MyHttpStatus.BUSINESS_ERROR, "操作失败");
    }


    /**
     * 上交成绩文件
     * @Auther doveness
     */

    @PostMapping("/uploadResultsFile")
    public Resp uploadResultsFile(MultipartFile resutls, @RequestParam("matchId") Integer matchId){
        /*1，判断文件合理性{
            1，文件为空
            2，文件合法
        }
        2，上传文件
        * 3,返回情况
        */

        if (resutls==null){
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"文件为空");
        }

        String fileName = resutls.getOriginalFilename();
        if (    fileName!=null
                && FileRight.isRight(fileName)){
            System.out.println("文件后缀没问题");
        }else {
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"文件后缀有问题");
        }



        Match match = matchService.getById(matchId);
        if (match.getResultStatus()!=null && !match.getResultStatus().equals("") ){
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"已经存放比赛成绩文件");
        }
//校验结束
        matchMapper.insertFile(matchMapper.selectById(matchId).getName() /*这是比赛名字*/ ,fileName);
        String message=null;
        message= FileUpload.uploadFile( resultPath, resutls);
        if (message.equals("上传成功")){

            match.setResultStatus(match.getId()+"r"); //比赛id+r代表  result——status，根据这个来获取文件


            matchService.updateById(match);


            return new Resp(MyHttpStatus.SUCCESS,message);
        }else {
            return new Resp(MyHttpStatus.BUSINESS_ERROR,message);
        }




    }


}
