package com.example.oj.controller;

import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.TeamUser;
import com.example.oj.entity.dao.Achievement;
import com.example.oj.entity.dao.Manage;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.UserVo;
import com.example.oj.mapper.AchievementMapper;
import com.example.oj.mapper.ManagerMapper;
import com.example.oj.mapper.connectMapper.TeamUserMapper;
import com.example.oj.service.UserService;
import com.example.oj.tool.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 良良
 * @date 2021/12/31 17:30
 */
@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * 注入Service
     * 注入邮箱验证码工具
     */
    private final UserService userService;
    private final EmailCode emailCode;
    private final TeamUserMapper teamUserMapper;
    private final RedisTemplate redisTemplate;
    private final ManagerMapper managerMapper;
    private final AchievementMapper achievementMapper;

    @Autowired
    public UserController(UserService userService,
                          EmailCode emailCode,
                          TeamUserMapper teamUserMapper,
                          RedisTemplate redisTemplate,
                          ManagerMapper managerMapper,
                          AchievementMapper achievementMapper){
        this.userService=userService;
        this.emailCode=emailCode;
        this.teamUserMapper=teamUserMapper;
        this.redisTemplate=redisTemplate;
        this.managerMapper = managerMapper;
        this.achievementMapper = achievementMapper;
    }

    /**
     * 登录接口
     */
    @PostMapping("/login")
    public ResponseEntity login(@RequestParam("username") String username,
                                @RequestParam("password") String password,
                                @RequestParam("code") String code,
                                @RequestParam("millis") String millis,
                                @RequestParam(value = "power",defaultValue = "2") int power){

        Manage manage = managerMapper.findPower(username);
        System.out.println("power="+power);
        if(power==1){
            System.out.println("manage="+manage);
            if(manage==null || !manage.getId().equals(power) ){
                return ResponseEntity.status(HttpStatus.OK).body(new Resp<>(MyHttpStatus.BUSINESS_ERROR,"您不是老师"));
            }
        }else if(power == 3){
            if(manage==null || !manage.getId().equals(power) ){
                return ResponseEntity.status(HttpStatus.OK).body(new Resp<>(MyHttpStatus.BUSINESS_ERROR,"您不是管理员"));
            }
        }



        String photoCode= (String)redisTemplate.opsForValue().get(millis);
        redisTemplate.delete(millis);
        System.out.println("1:"+photoCode);
        System.out.println("2:"+code);
        //判断图片验证码是否正确，忽略大小写
        if(photoCode.equalsIgnoreCase(code)){
            System.out.println("equal");
        }
        if(photoCode.equalsIgnoreCase(code)){

            //判断用户密码是否正确
            User user = userService.login(username, password);
            System.out.println(user);
            if(user!=null){



                //用来存放payload
                Map<String, String> tokenMap = new HashMap<>();
                tokenMap.put("id",user.getId().toString());
                tokenMap.put("username", user.getUsername());
                tokenMap.put("password",user.getPassword());

                //获取token令牌
                String token = MyJWT.getToken(tokenMap);

                System.out.println("token:"+token);
                //BaseUserInfo.set("id",user.getId().toString());

                Map<String, Object> reMap = new HashMap<>();
                reMap.put("id",user.getId());
                reMap.put("username",user.getUsername());
                //reMap.put("password",user.getPassword());
                reMap.put("email",user.getEmail());
                reMap.put("info",user.getInfo());
                TeamUser teamUser = userService.findTeam(user.getId());
                //判断是否为队长
                if(userService.findTeam(user.getId())==null) reMap.put("isCaptain",1);
                else reMap.put("isCaptain",0);
                //判断是否有队伍
                if(teamUserMapper.findByUserID(user.getId())!=null) reMap.put("hasGroup",0);
                else reMap.put("hasGroup",1);
                reMap.put("invitationCode",MD5.encryption(String.valueOf(user.getId()+123456)));
                reMap.put("token",token);

                return ResponseEntity.status(HttpStatus.OK).header(HttpHeaders.AUTHORIZATION,token).body(new Resp(MyHttpStatus.SUCCESS,"登陆成功",reMap));
            }else {
                return ResponseEntity.status(HttpStatus.OK).body(new Resp<>(MyHttpStatus.USER_ACCOUNT_ERROR,"用户名或密码错误"));
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Resp<>(MyHttpStatus.PARAM_ERROR,"验证码错误"));
    }

    /**
     * 图片验证码
     */
    @GetMapping("/photoCode")
    public Resp photoCode(HttpServletRequest request, HttpServletResponse response) throws IOException {

        //获取图片对象
        Photo.Validate randomCode = Photo.getRandomCode();
        System.out.println(randomCode.getValue());

        String millis = Long.toString(System.currentTimeMillis());
        System.out.println("millis: "+millis);
        redisTemplate.opsForValue().set(millis,randomCode.getValue());
        Map<String,Object> map =new HashMap<>();
        map.put("millis",millis);
        map.put("base64",randomCode.getBase64Str());
        return new Resp(200,map);

    }



    /**
     * 邮箱验证码
     */
    @PostMapping("/Code")
    public Resp Code(HttpSession session,@RequestParam("email") String email) throws Exception {

        //获取邮箱验证码
        String code = emailCode.code(email);

        System.out.println(code);

        String millis = Long.toString(System.currentTimeMillis());
        redisTemplate.opsForValue().set(millis,code);
        redisTemplate.expire(millis,5,TimeUnit.MINUTES);


        Map<String,Object> map =new HashMap<>();
        map.put("millis",millis);
        return new Resp<>(MyHttpStatus.SUCCESS,"验证码已发送",map);
    }


    /**
     * 注册接口
     */
    @PostMapping("/register")
    public Resp register(UserVo userVo){

        String code= (String)redisTemplate.opsForValue().get(userVo.getMillis());
        redisTemplate.delete(userVo.getMillis());
        System.out.println(code);
        //判断验证码是否正确，忽略大小写
        if(code!=null && code.equalsIgnoreCase(userVo.getEmailCode())){
            if(!userService.findByName(userVo.getUsername())){
                return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"用户名重复");
            }
            User user = userService.register(userVo);
            if(user != null ){
                return new Resp(MyHttpStatus.SUCCESS,"注册成功", new Object[]{user});
            }
            return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"邮箱已注册，忘记密码，请找回");
        }
        return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"验证码错误，或失效");

    }

    /**
     * 找回密码
     */
    @PostMapping("/retrieve")
    public Resp retrieve(
                         @RequestParam("retrieveEmail") String retrieveEmail,
                         @RequestParam("newPassword") String newPassword,
                         @RequestParam("retrieveCode") String retrieveCode,
                         @RequestParam("millis") String millis)  {


        String code= (String)redisTemplate.opsForValue().get(millis);
        redisTemplate.delete(millis);
        if(code!=null && code.equalsIgnoreCase(retrieveCode)){
            User user = userService.retrieve(retrieveEmail,newPassword);
            if(user != null ){
                return new Resp(MyHttpStatus.SUCCESS,"修改成功", new Object[]{user});
            }
            return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"邮箱未注册，请注册");
        }
        return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"验证码错误，或失效");
    }


    /**
     * 退出团队
     */
    @PostMapping("/exitTeam")
    public Resp exitTeam(@RequestParam("userId") int userId, @RequestParam("teamId") int teamId){
        userService.exitTeam(userId,teamId);
        Map<String,Object> map=new HashMap<>();

         map.put("isCaptain",1);
         map.put("hasGroup",1);
        return new Resp<>(MyHttpStatus.SUCCESS,"退出成功",map);
    }

    /**
     * 查询信息
     */
    @PostMapping("findAll")
    public Resp findAll(@RequestParam("userId") int userId){
        User user = userService.findAll(userId);
        if(user == null){
            return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"查询失败");
        }
        return new Resp<>(MyHttpStatus.SUCCESS, user);
    }
    /**
     * 修改信息
     */
    @PostMapping("updateInfo")
    public Resp update(User user) {
        if(userService.updateInfo(user)==1){
            return new Resp<>(MyHttpStatus.SUCCESS, "修改成功");
        }
        return new Resp<>(MyHttpStatus.BUSINESS_ERROR, "修改失败");
    }

    /**
     * 查询个人成就
     */
    @PostMapping("/findAchievement")
    public Resp findAchievement(@RequestParam("userId") int userId){
        Map<String, Object> map = new HashMap<>();
        map.put("user_id",userId);
        List<Achievement> achievements = achievementMapper.selectByMap(map);
        if(achievements!=null&&achievements.size() > 0){
            return new Resp<>(MyHttpStatus.SUCCESS,achievements);
        }
        return new Resp<>(MyHttpStatus.BUSINESS_ERROR,"没有成就");

    }


}
