package com.example.oj.controller;

import com.example.oj.entity.Resp;
import com.example.oj.entity.connect.SubjectStatusUser;
import com.example.oj.entity.dao.Subject;
import com.example.oj.entity.vo.SubjectCountVo;
import com.example.oj.entity.vo.SubjectVo;
import com.example.oj.service.SubjectService;
import com.example.oj.tool.MyHttpStatus;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/18 12:39
 */
@RestController
@RequestMapping("/subject")
public class SubjectController {

    private final SubjectService subjectService;
    @Autowired
    SubjectController(SubjectService subjectService){
        this.subjectService=subjectService;
    }

    /**
     * @author 良良
     * 题库导入
     */
    @PostMapping("/importSubject")
    public Resp importSubject(@RequestPart("file") MultipartFile file) throws IOException {
        boolean b = subjectService.importSubject(file);
        return new Resp(200,"导入成功");

    }

    /**
     * @author dovesess
     *  @date 2022/1/22 12:20
     * 用于获取单个题目题目详细信息
     */

    @PostMapping("/getDetails")
    public Resp getSubjectDetails(@RequestParam("sub_id") Integer sub_id){
        if (sub_id!=null){
            System.out.println(sub_id+"\n\n");
        }

        Subject subject = subjectService.getSubjectDetails(sub_id);
        if (subject!=null && subject.getId()!=null){
            return new Resp(MyHttpStatus.SUCCESS,"查询成功",subject);
        }else {
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"查无此题目");
        }
    }

    /**
     * @author dovesess
     *  @date 2022/1/22 12:20
     * 用于获取 -题库题目状态更新（未做、已做），查询结果为空则默人没做
     * 这个表没必要再建一个新类了，就一个查询
     * 0代表未作，1代表已做，默认为0
     */

    @PostMapping("/updateSubjectStatus")
    public Resp updateSubjectStatus(@RequestParam("userId") Integer userId,
                                    @RequestParam("sub_id") Integer sub_id,
                                    @RequestParam("status") Integer status){
        SubjectStatusUser subject_status_user  = subjectService.updateSubjectStatus(userId, sub_id, status);
        if (subject_status_user==null){
            System.out.println("对象是空的\n\n");
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"操作错误，有问题");
        }else if (subject_status_user.getStatusId()==null){
            System.out.println(subject_status_user.toString());
            System.out.println("id是空的\n\n");
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"操作错误，有问题");
        }else {
            return new Resp(MyHttpStatus.SUCCESS,"修改成功",subject_status_user);
        }
    }

    @PostMapping("/inquireSubject")
    public Resp inquireSubject(@RequestParam(value= "search",required = false) String key ,
                               @RequestParam("user_id") Integer user_id ,
                               @RequestParam("page") Integer page ,
                               @RequestParam("pageSize") Integer pageSize ,
                               @RequestParam(value= "status",required = false) Integer status ,
                               @RequestParam(value= "grade",required = false) Integer grade
    ){
        SubjectCountVo subjectVos = subjectService.inquireSubject(key, user_id, page, pageSize, status, grade);
        if(subjectVos==null||subjectVos.getSubjectVoList().isEmpty()) {

            return new Resp(MyHttpStatus.BUSINESS_ERROR, "无数据或出现问题");

        }
        else{
            return new Resp(MyHttpStatus.SUCCESS,"查询成功",subjectVos);
        }
    }

    @PostMapping("/deleteSubject")
    public Resp deleteSubject(@RequestParam("id") Integer id){
        if(subjectService.deleteSubject(id)){
            return new Resp(MyHttpStatus.SUCCESS,"删除成功");
        }else{
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"无数据或出现问题");
        }
    }

    @PostMapping("/updateSubject")
    public Resp updateSubject(Subject subject){
        if(subjectService.updateSubject(subject)){
            return new Resp(MyHttpStatus.SUCCESS,"修改成功");
        }else{
            return new Resp(MyHttpStatus.BUSINESS_ERROR,"无数据或出现问题");
        }
    }
}
