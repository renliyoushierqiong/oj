package com.example.oj.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 良良
 * @date 2022/1/2 21:38
 */
@Configuration
public class MySpringMvcConfig implements WebMvcConfigurer {



    /**
     * 自定义拦截器
     *
     */
    @Bean
    public MyHandler myHandler(){
        return new MyHandler();
    }

    @Bean
    public AdminHandler adminHandler() {
        return new AdminHandler();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        System.out.println(registry);
        registry.addInterceptor(myHandler())
                .addPathPatterns("/**")
                .excludePathPatterns("/user/photoCode",
                        "/user/login",
                        "/user/register",
                        "/user/Code",
                        "/user/retrieve",
                        "/img/**",
                        "/admin/**"
                        )
        ;
        registry.addInterceptor(adminHandler())
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/Rotation")
        ;
    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/img/**").addResourceLocations("file:/OJ/img/");

    }





}
