package com.example.oj.config;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.oj.entity.dao.Manage;
import com.example.oj.mapper.ManagerMapper;
import com.example.oj.tool.BaseUserInfo;
import com.example.oj.tool.MyJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 良良
 * @date 2022/2/12 18:21
 */
public class AdminHandler implements HandlerInterceptor {



    @Autowired
    private ManagerMapper managerMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{

        //获得请求头里的TOKEN数据
        String token = request.getHeader("Authorization").substring(7);

        //根据token解析数据，因为配置了全局异常处理中心，如果有异常会在全局异常中心处理异常
        DecodedJWT tokenInfo = MyJWT.getTokenInfo(token);
        //获得用户信息
        String name = tokenInfo.getClaim("username").asString();
        Manage manage = managerMapper.findPower(name);
        System.out.println("AOP:"+name);
        System.out.println(manage.getId());
        if(manage!=null&&manage.getId()!=3){
            System.out.println("应该被拦截");
            return false;
        }


        return true;

    }
}
