package com.example.oj.config;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.oj.tool.BaseUserInfo;
import com.example.oj.tool.MyJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/2 22:56
 */
public class MyHandler implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{

//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json; charset=utf-8");
//        response.setHeader("Access-Control-Allow-Credentials","true");
//        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
//        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//        response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");


        //获得请求头里的TOKEN数据
        String token = request.getHeader("Authorization").substring(7);



        //根据token解析数据，因为配置了全局异常处理中心，如果有异常会在全局异常中心处理异常
        DecodedJWT tokenInfo = MyJWT.getTokenInfo(token);

        //获得用户信息
        String name = tokenInfo.getClaim("username").asString();
        String pass = tokenInfo.getClaim("password").asString();



        //存储用户信息到ThreadLocal中
        BaseUserInfo.set("username",name);
        BaseUserInfo.set("password",pass);

        return true;

    }


}
