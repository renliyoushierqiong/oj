package com.example.oj.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.oj.entity.dao.Match;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:14
 */

public interface MatchService extends IService<Match> {
    List<Match> findMatch(int matchId);

    public String getFileNameByResultStatus(String resultStatus);//通过id找filename

    boolean applyMatch(Integer teamId,Integer matchId);

    boolean releaseMatch(Match match);

    boolean UploadMatchFile(MultipartFile file,String matchName);

    boolean judgeMatch(Match match);

    String findFile(String matchName);

    IPage<Match> findMatchPage(int i, String matchName, int page, int pageSize);

}
