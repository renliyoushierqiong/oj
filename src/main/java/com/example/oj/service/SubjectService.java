package com.example.oj.service;


import com.example.oj.entity.connect.SubjectStatusUser;
import com.example.oj.entity.dao.Subject;
import com.example.oj.entity.vo.SubjectCountVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/18 12:43
 */
public interface SubjectService {
    boolean importSubject(MultipartFile file) throws IOException;
    public Subject getSubjectDetails(Integer sub_id);

    public SubjectStatusUser updateSubjectStatus(Integer userId, Integer sub_id, Integer status);

    SubjectCountVo inquireSubject(String key , Integer id , Integer page, Integer pageSize , Integer status , Integer grade);

    int subjectCount(String key, Integer id , Integer status , Integer grade);

    boolean deleteSubject(Integer id);//id指的是题目id

    boolean updateSubject(Subject subject);//id指的是题目id

    Integer solveCount(Integer id);

    public Map<String, Object> listCheck(int page, int pageSize);//查询个人做题排行榜

    public Map<String, Object> listCheckTeam(int page, int pageSize);
}
