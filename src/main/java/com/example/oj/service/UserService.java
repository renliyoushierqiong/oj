package com.example.oj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.oj.entity.connect.TeamUser;
import com.example.oj.entity.dao.Match;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.UserVo;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:19
 */
public interface UserService extends IService<User> {
    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    User login(String username,String password);

    /**
     * 注册
     * @param userVo
     * @return
     */
    User register(UserVo userVo);
    Boolean findByName(String name);
    /**
     * 找回密码,设置新密码
     */
    User retrieve(String email,String password);

    /**
     * 查询是否有队伍
     */
    TeamUser findTeam(int userID);

    /**
     * 退出队伍
     *
     */
    void exitTeam(int userId,int teamId);

    /**
     * 查询个人信息
     */
    User findAll(int id);

    /**
     * 修改信息
     */
    int updateInfo(User user);

}
