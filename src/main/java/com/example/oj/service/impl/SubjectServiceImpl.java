package com.example.oj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.example.oj.entity.connect.SubjectLists;
import com.example.oj.entity.connect.SubjectStatusUser;
import com.example.oj.entity.dao.Subject;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.teamSubject.TeamSubjectInfo;
import com.example.oj.entity.vo.SubjectCountVo;
import com.example.oj.entity.vo.SubjectVo;
import com.example.oj.mapper.SubjectMapper;
import com.example.oj.mapper.UserMapper;
import com.example.oj.mapper.connectMapper.SubjectListsMapper;
import com.example.oj.mapper.connectMapper.SubjectStatusMapper;
import com.example.oj.service.SubjectService;
import com.example.oj.tool.ExcelAnalysis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/18 12:43
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectMapper subjectMapper;
    private final UserMapper userMapper;
    private final SubjectStatusMapper subjectStatusMapper;
    private final SubjectListsMapper subjectListsMapper;


    @Autowired
    public SubjectServiceImpl(SubjectMapper subjectMapper,
                              UserMapper userMapper ,
                              SubjectStatusMapper subjectStatusMapper,
                              SubjectListsMapper subjectListsMapper) {
        this.subjectMapper = subjectMapper;
        this.userMapper = userMapper;
        this.subjectStatusMapper = subjectStatusMapper;
        this.subjectListsMapper=subjectListsMapper;
    }

    /**
     * 题库导入
     * @param file
     * @return
     * @throws IOException
     */
    @Override
    public boolean importSubject(MultipartFile file) throws IOException {
        Subject[] subjects = ExcelAnalysis.analysisFile(file);
        for (Subject subject : subjects) {
            if (subject != null) {
                System.out.println(subject);
                subjectMapper.insert(subject);
            }
        }
        return true;
    }

    @Override
    public Subject getSubjectDetails(Integer sub_id) {
        return subjectMapper.selectById(sub_id);
    }


    /**
     * 修改题目状态
     *
     */

    @Override
    public SubjectStatusUser updateSubjectStatus(Integer userId, Integer sub_id, Integer status) {

        if (status!= SubjectStatusUser.finish && status!= SubjectStatusUser.notFinish){
            System.out.println("status有问题");
            return null;
        }


        User user = userMapper.selectById(userId);
         if (user ==null){
            System.out.println("没这个人");
            return null;
        }


        Subject subject  = subjectMapper.selectById(sub_id);
        if (subject!=null){
            System.out.println("一切正常，相安无事");
        }else {
            return null;
        }
/*  *************************上面是数据检查部分*****************************************************  */

        Map<String,Object> map=new HashMap<>();
        map.put("user_id",userId);
        map.put("sub_id",sub_id);
        map.put("status",status);
        map.put("nowTime", LocalDateTime.now());
        //修改的部分
        map.put("name",user.getName());


        SubjectStatusUser subject_status_user  = subjectMapper.selectThis(map);

        if (subject_status_user!=null){
            System.out.println(subject_status_user.toString());
        }
        if (subject_status_user!=null && subject_status_user.getStatusId()!=null){
            System.out.println("有对象，执行修改操作\n\n");
            subjectMapper.updateSubjectStatus(map);
        }else {
            System.out.println("没对象，执行添加操作");
            subjectMapper.insertSubjectStatus(map);
        }

        //TODO here
        //把统计做题数据的表数据+1或者-1
        QueryWrapper<SubjectLists> qw=new QueryWrapper<>();//查询条件 where userid=？ and username=?
        qw.eq("user_id",userId);
        qw.eq("user_name",user.getUsername());

        System.out.println("/*************现在开始查***********/");

        SubjectLists subjectLists = subjectListsMapper.selectOne(qw);
        SubjectStatusUser subjectStatusUser = subjectMapper.selectThis(map);


        if (subjectLists!=null && ( subject_status_user!=null && subject_status_user.getStatus().equals(status) )  ){
            System.out.println("状态和原来相等，不用管");
        }else if (subjectLists!=null && (subjectStatusUser!=null )){//做题统计表李有数据执行修改操作
            Integer Subnum=subjectLists.getSubjectNum();
            //System.out.println("/*********"+Subnum+"*****/");

            if (status==SubjectStatusUser.notFinish){
                subjectLists.setSubjectNum( (Subnum-1)  );
            }else if (status==SubjectStatusUser.finish){
                subjectLists.setSubjectNum( (Subnum+1)  );
            }
            subjectListsMapper.update(subjectLists,qw);

        }else if (subjectLists==null){
            //如果没有数据，，执行添加操作


            SubjectLists subjectListsInsert=new SubjectLists();

            if (status==SubjectStatusUser.notFinish){
                subjectListsInsert.setSubjectNum(0);
            }else if (status==SubjectStatusUser.finish){
                subjectListsInsert.setSubjectNum(1);
            }


            subjectListsInsert.setUsername(user.getUsername());
            subjectListsInsert.setUserid(userId);
            subjectListsInsert.setName(user.getName());
                subjectListsMapper.insert(subjectListsInsert);
            }

        return subjectStatusUser;
    }



    @Override
    public SubjectCountVo inquireSubject(String key, Integer id, Integer page, Integer pageSize , Integer status , Integer grade) {

        User user = userMapper.selectById(id);
        if (user==null){
            System.out.println("查无此人");
            return null;
        }
        System.out.println(status+"  "+grade);
        //Integer count=0;
        IPage<Subject> pages = new Page<>(page,pageSize);
        List<SubjectVo> list = new ArrayList<>();//最后返回的结果
        QueryWrapper<Subject> qw = new QueryWrapper<>();
        if(grade!=null)qw.eq("grade",grade);

        if(key == null /*|| key.isEmpty()*/ ){

            IPage<Subject> subjectIPage = subjectMapper.selectPage(pages, qw);
            List<Subject> records = subjectIPage.getRecords();
            for (Subject record : records) {
                SubjectVo subjectVo = new SubjectVo();//将数据一个一个赋给SubjectVo
                subjectVo.setSubject_id(record.getId());
                subjectVo.setSubject_name(record.getName());
                //subjectVo.setDescribes(record.getDescribes());
                subjectVo.setGrade(record.getGrade());
                subjectVo.setTime(record.getTime());
                subjectVo.setSolveMan(solveCount(record.getId()));

                //查询状态
                Map<String,Object> map = new HashMap<>();
                map.put("user_id",id);
                map.put("subject_id",record.getId());
                List<SubjectStatusUser> subjectStatusUsers = subjectStatusMapper.selectByMap(map);
                if(subjectStatusUsers.isEmpty())//单独判断状态，如果status表中没有，就赋默认值0
                    subjectVo.setStatus(0);
                else
                    subjectVo.setStatus(subjectStatusUsers.get(0).getStatus());

                //将对象放入list结果中
                if(status==null){
                    list.add(subjectVo);
                    //count++;
                }
                else{
                    if(subjectVo.getStatus()==status){
                        list.add(subjectVo);
                        //count++;
                    }
                }

            }

        }else{
            qw.like("name",key);
            IPage<Subject> subjectIPage = subjectMapper.selectPage(pages, qw);
            List<Subject> records = subjectIPage.getRecords();
            for (Subject record : records) {
                SubjectVo subjectVo = new SubjectVo();//将数据一个一个赋给SubjectVo
                subjectVo.setSubject_id(record.getId());
                subjectVo.setSubject_name(record.getName());
                //subjectVo.setDescribes(record.getDescribes());
                subjectVo.setGrade(record.getGrade());
                subjectVo.setTime(record.getTime());
                subjectVo.setSolveMan(solveCount(record.getId()));

                //查询状态
                Map<String,Object> map = new HashMap<>();
                map.put("user_id",id);
                map.put("subject_id",record.getId());
                List<SubjectStatusUser> subjectStatusUsers = subjectStatusMapper.selectByMap(map);
                if(subjectStatusUsers.isEmpty())//单独判断状态，如果status表中没有，就赋默认值0
                    subjectVo.setStatus(0);
                else
                    subjectVo.setStatus(subjectStatusUsers.get(0).getStatus());

                //将对象放入list结果中
                if(status==null){
                    list.add(subjectVo);
                    //count++;
                }
                else{
                    if(subjectVo.getStatus()==status){
                        list.add(subjectVo);
                        //count++;
                    }
                }

            }
        }

        return new SubjectCountVo(list,subjectCount(key,id,status,grade));
    }
    public int subjectCount(String key, Integer id , Integer status , Integer grade){
        QueryWrapper<Subject> qw = new QueryWrapper<>();
        if(key!=null)qw.like("name",key);
        if(grade!=null)qw.eq("grade",grade);
        Integer result = subjectMapper.selectCount(qw);

        if(status==null) return result;
        else{
            int count=0;
            List<Subject> subjects = subjectMapper.selectList(qw);
            for (Subject subject : subjects) {
                //查询状态
                Map<String,Object> map = new HashMap<>();
                map.put("user_id",id);
                map.put("subject_id",subject.getId());
                List<SubjectStatusUser> subjectStatusUsers = subjectStatusMapper.selectByMap(map);
                if(status==0){
                    if(subjectStatusUsers.isEmpty()||subjectStatusUsers.get(0).getStatus()==0)
                        count++;
                }else {
                    if(!subjectStatusUsers.isEmpty()&&subjectStatusUsers.get(0).getStatus()==1)
                        count++;
                }
            }
            return count;
        }

    }

    @Override
    public boolean deleteSubject(Integer id) {
        int result = subjectMapper.deleteById(id);
        return result != -1;

    }

    @Override
    public boolean updateSubject(Subject subject) {//1是成功，0是无当前id,-1是修改失败
        int result = subjectMapper.updateById(subject);
        return result != -1;
    }

    @Override
    public Integer solveCount(Integer id) {//查询每一个题的解出人数
        QueryWrapper<SubjectStatusUser> qw = new QueryWrapper<>();
        qw.eq("subject_id",id);
        qw.eq("status",1);
        Integer integer = subjectStatusMapper.selectCount(qw);
        return integer;
    }

    /**
     * 查询排行榜--个人
     * @return
     */

    @Override
    public Map<String, Object> listCheck(int page, int pageSize){

        QueryWrapper<SubjectLists> qw=new QueryWrapper<>();
        qw.orderByDesc("subject_num");//依据做题数量查询

        IPage<SubjectLists> pages =new Page<>(page,pageSize);
        IPage<SubjectLists> listsIPage = subjectListsMapper.selectPage(pages,qw);

        List<SubjectLists> records = listsIPage.getRecords();
        long total = listsIPage.getTotal();

        Map<String,Object> map=new HashMap<>();

        map.put("total",total);
        map.put("datas",records);


        return map;


    }
/**
 * 查询排行榜--团队
 * @return
 */

@Override
public Map<String, Object> listCheckTeam(int page, int pageSize){

   Map<String,Object> data=new HashMap<>();



   data.put("page", (page-1)*pageSize);
   data.put("num",pageSize);


    List<TeamSubjectInfo> teamSubjectInfos = subjectListsMapper.selectTeamSubNum(data);

    //System.out.println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"+subjectListsMapper.teamAll().size()+"xxxxxxxxxxxxxxxxxxxx");

    List<Integer> list = subjectListsMapper.teamAll();
    Map<String,Object> back=new HashMap<>();

    back.put("total",list.size());
    back.put("datas",teamSubjectInfos);
    return back;

}


}
