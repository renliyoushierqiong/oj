package com.example.oj.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.oj.entity.connect.MatchUserTeam;
import com.example.oj.entity.dao.Match;
import com.example.oj.entity.dao.MatchFile;
import com.example.oj.entity.dao.Team;
import com.example.oj.entity.dao.User;
import com.example.oj.mapper.MatchFIleMapper;
import com.example.oj.mapper.MatchMapper;
import com.example.oj.mapper.TeamMapper;
import com.example.oj.mapper.UserMapper;
import com.example.oj.mapper.connectMapper.MatchUserTeamMapper;
import com.example.oj.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:16
 */
@Service
public class MatchServiceImpl extends ServiceImpl<MatchMapper, Match> implements MatchService {
    private  final MatchMapper matchMapper;
    private  final TeamMapper teamMapper;
    private  final MatchUserTeamMapper matchUserTeamMapper;
    private  final MatchFIleMapper mapper;

    @Autowired
    public MatchServiceImpl(MatchMapper matchMapper,TeamMapper teamMapper,MatchUserTeamMapper matchUserTeamMapper , MatchFIleMapper mapper){
        this.matchMapper=matchMapper;
        this.teamMapper=teamMapper;
        this.matchUserTeamMapper=matchUserTeamMapper;
        this.mapper=mapper;
    }

    @Override
    public List<Match> findMatch(int matchId) {
        List<Match> matches = matchMapper.findAll(matchId);
        return matches;
    }

    @Override
    public String getFileNameByResultStatus(String resultStatus){
        String idToFile = matchMapper.getIdToFile(resultStatus);
        return idToFile;

    }



    @Override
    public boolean applyMatch(Integer teamId, Integer matchId) {
        Match match = matchMapper.selectById(matchId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = new Date();
            Date startDate = format.parse(match.getSignStart());
            Date endDate = format.parse(match.getSignEnd());
            if(!startDate.before(date) || !endDate.after(date)){
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Map<String,Object> map = new HashMap<>();
        map.put("id",teamId);
        List<Team> teams = teamMapper.selectByMap(map);
        MatchUserTeam matchUserTeam = new MatchUserTeam();
        matchUserTeam.setMatch_id(matchId);
        matchUserTeam.setTeam_id(teamId);
        matchUserTeam.setUser_id(teams.get(0).getCaptionId());
        matchUserTeam.setTeam_name(teams.get(0).getName());

        Integer insert = matchUserTeamMapper.insert(matchUserTeam);
        //更新团队中的比赛id
        teamMapper.updateMatchId(matchId,teamId);
        if(insert==1) return true;
        else
            return false;
    }

    @Override
    public boolean releaseMatch(Match match) {

        int insert = matchMapper.insert(match);
        if (insert == 1) {
            return true;
        }
        return false;
    }

    @Override
    public boolean UploadMatchFile(MultipartFile file,String matchName){
        String path = "/OJ/match";
        String fileName = file.getOriginalFilename();
        File fileParent = new File(path);
        if (!fileParent.exists()) {
            fileParent.mkdirs();
        }
        File f = new File(path+"/",fileName);
        try {
            if(!f.exists()){
                f.createNewFile();
            }
            file.transferTo(f);
        }catch (IOException e) {
            return false;
        }
        matchMapper.insertFile(matchName,fileName);
//        MatchFile matchFile = new MatchFile(matchName,fileName);
//        mapper.insert(matchFile);
        return true;
    }

    /**
     * 审核比赛
     * @param match
     * @return
     */
    @Override
    public boolean judgeMatch(Match match) {
        int i = matchMapper.updateById(match);
        if (i == 1) {
            return true;
        }
        return false;
    }

    @Override
    public String findFile(String matchName) {
        return matchMapper.findFile(matchName);
    }

    @Override
    public IPage<Match> findMatchPage(int i,String matchName,int page,int pageSize) {
        Page<Match> page1 = new Page<>(page, pageSize);
        Map<String,Object> map = new HashMap<>();
        if(i!=0) {
            map.put("status",i);
        }
        map.put("matchName",matchName);

        IPage<Match> matchPage = matchMapper.findMatchPage(page1, map);
        return matchPage;
    }



}
