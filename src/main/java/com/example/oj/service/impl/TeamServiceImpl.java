package com.example.oj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.oj.entity.dao.Team;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.TeamVo;
import com.example.oj.mapper.MatchMapper;
import com.example.oj.mapper.TeamMapper;
import com.example.oj.mapper.UserMapper;
import com.example.oj.mapper.connectMapper.MatchUserTeamMapper;
import com.example.oj.mapper.connectMapper.TeamUserMapper;
import com.example.oj.service.TeamService;
import com.example.oj.tool.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:20
 */
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team> implements TeamService {

    private final TeamMapper teamMapper;
    private final UserMapper userMapper;
    private final TeamUserMapper teamUserMapper;
    private final MatchMapper matchMapper;// TODO 组建了队伍比的是哪场比赛
    private final MatchUserTeamMapper matchUserTeamMapper;


    @Autowired
    public TeamServiceImpl(TeamMapper teamMapper,
                           UserMapper userMapper,
                           TeamUserMapper teamUserMapper,
                           MatchMapper matchMapper,
                           MatchUserTeamMapper matchUserTeamMapper){
        this.teamMapper=teamMapper;
        this.userMapper=userMapper;
        this.teamUserMapper=teamUserMapper;
        this.matchMapper=matchMapper;
        this.matchUserTeamMapper=matchUserTeamMapper;

    }

    /**
     * 创建队伍
     * @param team
     * @return
     */
    @Override
    public boolean creatTeam(TeamVo team) {


        //判断此人是否有队伍
        Map<String,Object> map=new HashMap<>();
        map.put("captionId",team.getCaptionId());
        List<Team> teams = teamMapper.selectByMap(map);


        //没有队伍时，创建队伍
        if(teams==null||teams.isEmpty()){
            Team teamResult = new Team();
            teamResult.setName(team.getName());
            teamResult.setCaptionId(team.getCaptionId());
            teamResult.setDescribes(team.getDescribe());
            //teamResult.setDelete(0);

            //在team表添加队伍信息
             teamMapper.insert(teamResult);

            //利用sql语句在team表和user表查询数据后，插入到team_user表中
            teamUserMapper.findByIdInsertTeam(team.getCaptionId());
            return true;
        }
        return false;

    }

    /**
     *删除队伍
     */
    @Override
    public boolean deleteTeam(Integer captionId) {
        teamUserMapper.deleteByUser_id(captionId);
        teamMapper.delByCaptionId(captionId);
        return true;
    }

    /**
     * 查询队伍信息
     * @param name
     * @return
     */
    @Override
    public Team findAll(String name) {
        Team team = teamMapper.findAllByName(name);
        return team;
    }

    @Override
    public Team findAll(Integer userId) {
        Team team = teamMapper.findAllByUserId(userId);
        if(team!=null){
            return team;
        }
        return null;

    }


    public boolean isCaption(Integer id){
        Team team = teamMapper.selectByCaptionId(id);
        if(team == null){
            return true;
        }
        return false;
    }

    /**
     * @Auther dovesess
     * @Data 2022.1.6 17:26
     * 增添队员考虑问题
     * 1，a是不是队长
     * 2，有这个人 b,   a(有待考虑，感觉不用)
     * 3，队里有没有这个人(不能重复添加)
     * 4，b在不在别的队伍里
     * @param invitationCode
     * @param teamId
     * @return
     */

    @Override
    public boolean addMember(String invitationCode,Integer teamId) {
        Integer memberId = Integer.parseInt(MD5.JM(invitationCode))-123456;

       User willAdd = userMapper.selectById(memberId);//即将添加的人
       Team team = teamMapper.selectById(teamId);
       if (team==null){
            System.out.println("没有这个队伍");
            return false;
       }
       if (willAdd==null){ //没有这个人
            System.out.println("没有这个人");
              return false;
       }
       //队伍满员  查询得到的人数大于比赛所限制的最大的人数
        Integer maxMan;
        if(matchUserTeamMapper.selectByTeam_id(teamId)!= null ){
            maxMan=matchMapper.selectById(team.getMatchId()).getNumberMan();
        }else {
            maxMan=10;
        }

        Integer nowMan=findMember(teamId).size();//
        if (nowMan>=maxMan){
            System.out.println("人满了，换一个吧");
            return false;
        }
                //通过这个人找他所在的队伍，若他所在的队伍为空，则继续
                Integer teamId_His=userMapper.findHisTeamId(memberId);//他所属的 team 的 id 为空,说明他不属于任何队伍
                if (teamId_His==null){
                   //执行添加
                    Map<String,Object> datas=new HashMap<>();
                    datas.put("userId",memberId);
                    datas.put("teamId",teamId);
                    datas.put("userName",willAdd.getUsername());
                    datas.put("isCaption",false);
                    teamMapper.insertInTeam(datas);

                    return true;
                }else {
                    if (teamId_His.equals(teamId)){
                        System.out.println("他已经是你得队伍的人了");//本想着来测验一下不同的情况，但没想到直接跳转到异常的情况
                    }else {
                        System.out.println("他已经属于别的队伍了");
                    }



                    return false;
                }

    }


    @Override
    public List<User> findMember(Integer teamId){
        List<User> users = teamUserMapper.findManInTeam(teamId);
        return users;
    }

    public Integer deleteMember( Integer UserId, Integer teamId){
        if(teamMapper.selectByCaptionId(UserId)!=null){
            return 0;
        }
        Map<String,Object> map=new HashMap<>();
        map.put("user_id",UserId);
        map.put("team_id",teamId);
        Integer integer = teamUserMapper.deleteByMap(map);
        return integer;

    }
}
