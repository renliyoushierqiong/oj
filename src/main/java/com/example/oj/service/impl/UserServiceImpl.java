package com.example.oj.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.oj.entity.connect.TeamUser;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.UserVo;
import com.example.oj.mapper.UserMapper;
import com.example.oj.mapper.connectMapper.TeamUserMapper;
import com.example.oj.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 注入userMapper实例
     */
    private final UserMapper userMapper;
    private final TeamUserMapper teamUserMapper;
    @Autowired
    public UserServiceImpl(UserMapper userMapper,TeamUserMapper teamUserMapper){
        this.userMapper=userMapper;
        this.teamUserMapper=teamUserMapper;
    }

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    @Override
    public User login(String username, String password) {
        //将信息存入map
        Map<String,Object> map = new HashMap<>();
        map.put("username",username);
        map.put("password",password);
        //查找用户信息
        List<User> users = userMapper.selectByMap(map);
        //判断list中是否为空，并返回查找值
        if(users == null || users.isEmpty()){
            return null;
        }else {
            return users.get(0);
        }
    }

    /**
     * 注册
     * @param userVo
     * @return
     */
    @Override
    public User register(UserVo userVo) {

        //将数据存入到User对象
        User user = new User();
        user.setEmail(userVo.getEmail());
        user.setUsername(userVo.getUsername());
        user.setPassword(userVo.getPassword());

        //根据邮箱查找是否被注册
        User byEmail = userMapper.findByEmail(userVo.getEmail());
        if(byEmail == null ){
            userMapper.insert(user);
            return user;
        }
        return null;
    }
    @Override
    public Boolean findByName(String name){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",name);
        List<User> users = userMapper.selectList(queryWrapper);
        return users.isEmpty();
        //根据username查找是否重复
//        String byName = userMapper.findByName(name);
//        return byName==null;
    }

    /**
     * 找回密码
     * @param email
     * @param password
     * @return
     */
    @Override
    public User retrieve(String email,String password) {
        User user = userMapper.findByEmail(email);
        if(user != null){
            user.setPassword(password);
            userMapper.updateById(user);
            return user;
        }
        return null;
    }

    @Override
    public TeamUser findTeam(int userID) {
        TeamUser byUserID = teamUserMapper.findByUserID(userID);
        if(byUserID!=null){
            if(byUserID.isCaptain()){
                return byUserID;
            }
        }

        return null;
    }

    /**
     * 队员退出团队
     * @author Jeckie
     * @return
     */
    @Override
    public void exitTeam(int userId,int teamId) {
        userMapper.ExitTeam(userId,teamId);
    }

    /**
     * 查询所有信息
     */
    @Override
    public User findAll(int id) {
        return userMapper.selectById(id);
    }


    @Override
    public int updateInfo(User user) {
        int update = userMapper.updateById(user);
        return update;
    }


}
