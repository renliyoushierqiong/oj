package com.example.oj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.oj.entity.dao.Match;
import com.example.oj.entity.dao.Team;
import com.example.oj.entity.dao.User;
import com.example.oj.entity.vo.TeamVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/3 10:18
 */

public interface TeamService extends IService<Team> {
    boolean creatTeam(TeamVo team);

    boolean deleteTeam(Integer captionId);

    Team findAll(String name);

    Team findAll(Integer userId);


    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    boolean addMember(String invitationCode,Integer teamId);

     List<User> findMember(Integer teamId);

    Integer deleteMember( Integer UserId, Integer teamId);
}
