package com.example.oj.tool;
import java.io.IOException;
import java.io.InputStream;

import com.example.oj.entity.dao.Subject;
import org.apache.poi.hssf.record.formula.functions.T;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 良良
 * @date 2022/1/16 23:21
 */
public class ExcelAnalysis {
   public static Subject[] analysisFile(MultipartFile file) throws IOException {

       //定义一个题目变量
       Subject[] subjects=null;
       //获取文件后缀
       String originalFilename = file.getOriginalFilename();
       //获取输入流
       InputStream inputStream = file.getInputStream();

       Workbook workbook=null;
       //根据文件类型创建相应的workbook
       if(originalFilename.endsWith("xls")){
            workbook = new HSSFWorkbook(inputStream);
       }else if (originalFilename.endsWith("xlsx")){
            workbook = new XSSFWorkbook(inputStream);
       }
        //获取文件中单元页
       for (int i=0;i<workbook.getNumberOfSheets();i++){
           //获取文件中单元页
           Sheet sheet = workbook.getSheetAt(i);
           //获取文件横向数据条数
           int number = sheet.getPhysicalNumberOfRows()-1;
           //每条数据都创建一个新的subject对象用来储存
           subjects=new Subject[number];
           for (int j=1; j<=number;j++){
               subjects[j-1]=new Subject();
               Row row = sheet.getRow(j);

               if(row != null){
                   for (int k=1;k<9;k++){
                       Cell cell = row.getCell(k);

                       if(cell!=null){

                           switch (k){
                               case 1:
                                   subjects[j-1].setName(cell.getRichStringCellValue().getString());
                                   break;
                               case 2:
                                   subjects[j-1].setDescribes(cell.getRichStringCellValue().getString());
                                   break;
                               case 3:
                                   subjects[j-1].setAnswer(cell.getRichStringCellValue().getString());
                                   break;
                               case 4:
                                   subjects[j-1].setPositions(cell.getRichStringCellValue().getString());
                                   break;
                               case 5:
                                   subjects[j-1].setGrade(new Double(cell.getNumericCellValue()).intValue());
                                   break;
                               case 6:
                                   subjects[j-1].setMatchId(new Double(cell.getNumericCellValue()).intValue());
                                   break;
                               case 7:
                                   subjects[j-1].setInput(cell.getRichStringCellValue().getString());
                                   break;
                               case 8:
                                   subjects[j-1].setOutput(cell.getRichStringCellValue().getString());
                                   break;
                               case 9:
                                   subjects[j-1].setInput1(cell.getRichStringCellValue().getString());
                                   break;
                               case 10:
                                   subjects[j-1].setOutput1(cell.getRichStringCellValue().getString());
                               case 11:
                                   subjects[j-1].setTime(cell.getRichStringCellValue().getString());

                           }
                       }


                   }
               }
           }

       }
       return subjects;
   }

}
