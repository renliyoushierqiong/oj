package com.example.oj.tool;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.example.oj.entity.Resp;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/3 0:27
 */
@Configuration
@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
public class GlobalException {
    @ExceptionHandler(SignatureVerificationException.class)
    public Resp testException(SignatureVerificationException signatureVerificationException) {
        return new Resp(MyHttpStatus.USER_LOGIN_ERROR,"签名异常此用户为登录，请登录");
    }

    @ExceptionHandler(TokenExpiredException.class)
    public Resp testException(TokenExpiredException tokenExpiredException) {
        return new Resp(MyHttpStatus.USER_LOGIN_ERROR,"token过期,此用户为登录，请登录");

    }

    @ExceptionHandler(AlgorithmMismatchException.class)
    public Resp testException(AlgorithmMismatchException AlgorithmMismatchException) {
        return new Resp(MyHttpStatus.USER_LOGIN_ERROR,"加密算法不匹配,此用户为登录，请登录");

    }
//    @ExceptionHandler(IOException.class)
//    public Resp testException(IOException ioException){
//        return new Resp(MyHttpStatus.BUSINESS_ERROR,"导入失败");
//    }

//    @ExceptionHandler(Exception.class)
//    public Resp testException(Exception exception) {
//        return new Resp(MyHttpStatus.USER_LOGIN_ERROR,"出错了");
//    }
}
