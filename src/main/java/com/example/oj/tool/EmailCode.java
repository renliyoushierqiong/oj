package com.example.oj.tool;

import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author 良良
 * @date 2022/1/1 13:37
 * 邮箱验证码工具
 */
@Component
@PropertySource("classpath:emailConfig.properties")
public class EmailCode {

    //发送者邮箱
    @Value("${email.sender}")
    private String sender;

    //设置服务器主机
    @Value("${email.host}")
    private String host;

    //授权码
    @Value("${email.SMTP}")
    private String SMTP;



    public String code(String recipient) throws Exception {

        Properties prop = new Properties();
        // 开启debug调试，以便在控制台查看
        prop.setProperty("mail.debug", "true");
        // 设置邮件服务器主机名
        prop.setProperty("mail.host", host);
        // 发送服务器需要身份验证
        prop.setProperty("mail.smtp.auth", "true");
        // 发送邮件协议名称
        prop.setProperty("mail.transport.protocol", "smtp");
        // 开启SSL加密，否则会失败
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);
        // 创建session
        Session session = Session.getInstance(prop);
        // 通过session得到transport对象
        Transport ts = session.getTransport();
        // 连接邮件服务器：邮箱类型，帐号，POP3/SMTP协议授权码
        ts.connect(host, sender, SMTP);
        // 创建邮件
        Message message = new MimeMessage(session);
        // 指明邮件的发件人
        message.setFrom(new InternetAddress(sender));
        // 指明邮件的收件人，发件人和收件人如果是一样的，那就是自己给自己发
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        // 邮件的标题
        message.setSubject("SYLU验证码");
        // 邮件的文本内容
        String stringBuilder = stringBuilder();
        message.setContent("欢迎您注册沈阳理工大学在线比赛系统,账号注册验证码为(十分钟有效):  "+stringBuilder+"    ,请勿回复此邮箱", "text/html;charset=UTF-8");
        // 发送邮件
        ts.sendMessage(message, message.getAllRecipients());
        ts.close();

        return stringBuilder;
    }
    private String stringBuilder(){
        //  获取6为随机验证码
        String[] letters = new String[] {
                "q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m",
                "A","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M",
                "0","1","2","3","4","5","6","7","8","9"};
        String stringBuilder ="";
        for (int i = 0; i < 6; i++) {
            stringBuilder = stringBuilder + letters[(int)Math.floor(Math.random()*letters.length)];
        }
        return stringBuilder;
    }



}
