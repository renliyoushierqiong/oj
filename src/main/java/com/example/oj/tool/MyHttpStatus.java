package com.example.oj.tool;

/**
 * @author 良良
 * @date 2021/12/31 17:17
 * 状态码工具
 *     SUCCESS: 200,
 *     PARAM_ERROR: 10001,//'参数不正确',
 *     USER_ACCOUNT_ERROR:20001,//用户账号密码错误
 *     USER_LOGIN_ERROR:20002,//用户未登录
 *     BUSINESS_ERROR: 30001,//业务请求失败
 *     AUTH_ERROR: 40001,//认证失败或TOKEN过期
 */
public  class MyHttpStatus {
    public static final int SUCCESS = 200;
    public static final int PARAM_ERROR = 10001;
    public static final int USER_ACCOUNT_ERROR = 20001;
    public static final int USER_LOGIN_ERROR = 20002;
    public static final int BUSINESS_ERROR = 30001;
    public static final int AUTH_ERROR = 40001;
}
