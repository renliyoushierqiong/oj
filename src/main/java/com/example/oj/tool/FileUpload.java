package com.example.oj.tool;

import com.example.oj.entity.Resp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
 * @author 良良
 * @date 2022/2/11 19:45
 */
@Slf4j

public class FileUpload {



     String staticPath = this.getClass().getClassLoader().getResource("static/img").getFile();

    public static void Upload(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();
        String realPath = ResourceUtils.getURL("classpath:").getPath() + "static/img";

        file.transferTo(new File(realPath+filename));
        System.out.println("成功");
    }

    public static void downloadFile(String place, String fileName, HttpServletResponse response) throws UnsupportedEncodingException {

        if(fileName!=null){

            File file = new File(place,fileName);
            log.info("文件路径：   {}",file.getAbsolutePath());
            if (file.exists()) {
                response.reset();
                response.setContentType("application/octet-stream;charset=utf-8");
                response.addHeader("Content-Disposition", "attachment;fileName=" + fileName +";filename*=utf-8''"+ URLEncoder.encode(fileName,"utf-8"));
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();

                    while (   bis.read(buffer) != -1) {
                        os.write(buffer, 0, buffer.length);
                    }

                    log.info("传输完成");
                    return ;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally { // 做关闭操作
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        log.error("传输文件失败");
        return;
    }



    public static String uploadFile( String place,MultipartFile results){

        //已经验证过合法
        File placeFile=new File(place);
        if (!placeFile.exists()){
            placeFile.mkdirs();//保险起见
        }

        File save=new File(place,results.getOriginalFilename());

        try {
            results.transferTo(save);
            return "上传成功";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "上传失败";

    }



}
