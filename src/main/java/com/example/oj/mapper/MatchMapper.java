package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.example.oj.entity.dao.Match;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.sql.Wrapper;
import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2021/12/31 17:23
 */
@Mapper
public interface MatchMapper extends BaseMapper<Match> {
    public List<Match> findAll(@Param("delete")int i);
    void insertFile(@Param("matchName") String matchName,@Param("fileName") String fileName);

    void insertMactchFile(@Param("matchName") String matchName,@Param("fileName") String fileName);

    String findFile(@Param("matchName") String matchName);

    //IPage<Match> findMatchPage(IPage<Match> page,int status,String matchName);
    IPage<Match> findMatchPage(IPage<Match> page, Map<String,Object> map);


    public String getIdToFile(@Param("resultStatus") String resultStatus);

}
