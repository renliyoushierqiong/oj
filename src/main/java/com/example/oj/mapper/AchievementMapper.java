package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.Achievement;
import com.example.oj.entity.dao.Manage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 良良
 * @date 2022/2/18 14:49
 */
@Mapper
public interface AchievementMapper extends BaseMapper<Achievement> {
    List<Achievement> findAll();
    void bindingAchievement(Achievement achievement);
    int findBySno(int sno);
}
