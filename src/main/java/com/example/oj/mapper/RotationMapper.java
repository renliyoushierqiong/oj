package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.oj.entity.dao.Rotation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author 良良
 * @date 2022/2/11 23:17
 */
@Mapper
public interface RotationMapper extends BaseMapper<Rotation> {
    public List<Rotation> findAll();

    void delete(@Param("name") String name);
}
