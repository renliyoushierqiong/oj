package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.MatchFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author 人生若只如初见
 * @Date 2022/4/21 21:43
 */
@Mapper
public interface MatchFIleMapper extends BaseMapper<MatchFile> {
}
