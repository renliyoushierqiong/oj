package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.Team;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author 良良
 * @date 2021/12/31 17:23
 */
@Mapper
public interface TeamMapper extends BaseMapper<Team> {
    void insertInTeam(Map<String,Object> data);
    Team selectByCaptionId (@Param("c_id") int id);
    void updateMatchId (@Param("matchId") int matchId,@Param("id") int id);
    void delByCaptionId(@Param("captionId") int c_id);
    Team findAllByName(@Param("name") String name);

    Team findAllByUserId(@Param("userId") Integer id);

//
//    @Select("SELECT id, name, captionId, `describe`, matchId, ranking, `delete`, instructor_name\n" +
//            "FROM team where captionId= #{captionId}")
//    List<Team> selectBycaptionId(@Param("captionId") Integer captionId);
}
