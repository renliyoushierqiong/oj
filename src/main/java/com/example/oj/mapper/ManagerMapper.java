package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.Manage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author 良良
 * @date 2021/12/31 17:22
 */
@Mapper
public interface ManagerMapper extends BaseMapper<Manage> {
    Manage findPower(@Param("name") String name);
}
