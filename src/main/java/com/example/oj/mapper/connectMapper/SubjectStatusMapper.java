package com.example.oj.mapper.connectMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.connect.SubjectStatusUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * @author 良良
 * @date 2022/1/23 15:56
 */
@Mapper
public interface SubjectStatusMapper extends BaseMapper<SubjectStatusUser> {
    @Select("select * from subject_status where subject_status.status_id=#{id}")
    SubjectStatusUser selectById(int id);

    SubjectStatusUser selectThisTest(Map<String,Object> datas);
}
