package com.example.oj.mapper.connectMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.connect.SubjectLists;
import com.example.oj.entity.teamSubject.TeamSubjectInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.relational.core.sql.In;

import java.util.List;
import java.util.Map;

@Mapper
public interface SubjectListsMapper extends BaseMapper<SubjectLists> {

    public List<TeamSubjectInfo> selectTeamSubNum(Map<String,Object> data);

    @Select("SELECT count(team_id)\n" +
            "FROM subject_lists,team_user \n" +
            "WHERE team_user .user_id =subject_lists .user_id \n" +
            "group by team_id ;\n")
    public List<Integer> teamAll();

}
