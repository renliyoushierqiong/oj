package com.example.oj.mapper.connectMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.example.oj.entity.connect.MatchUserTeam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.relational.core.sql.In;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/6 17:19
 */
@Mapper
public interface MatchUserTeamMapper extends BaseMapper<MatchUserTeam> {
    void deleteByUser_id (@Param("user_id") int userID);
    int selectByUser_id(@Param("user_id") int userID);
    Integer selectByTeam_id(@Param("team_id") int teamID);
}
