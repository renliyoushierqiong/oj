package com.example.oj.mapper.connectMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.connect.UserAchievement;

import org.apache.ibatis.annotations.Mapper;

/**
 * @author 良良
 * @date 2022/2/18 16:14
 */
@Mapper
public interface UserAchievementMapper extends BaseMapper<UserAchievement> {

}
