package com.example.oj.mapper.connectMapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.connect.TeamUser;
import com.example.oj.entity.dao.Action;
import com.example.oj.entity.dao.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.relational.core.sql.In;

import java.util.List;

/**
 * @author 良良
 * @date 2022/1/5 23:09
 */
@Mapper
public interface TeamUserMapper extends BaseMapper<TeamUser> {
    TeamUser findByUserID(int userID);
    void findByIdInsertTeam(@Param("userID") int userID);
    void deleteByUser_id(@Param("userID") int userID);

    List<User> findManInTeam(@Param("teamId")Integer teamId);

}
