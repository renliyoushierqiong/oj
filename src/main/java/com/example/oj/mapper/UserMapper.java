package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.relational.core.sql.In;

/**
 * @author 良良
 * @date 2021/12/31 17:23
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    User findByEmail(@Param("email") String email);

    String findByName(@Param("username") String username);

    void ExitTeam(@Param("userId")int userId,@Param("teamId") int teamId);

    Integer findHisTeamId(@Param("memberId") Integer memberId); //这一个注解真离谱
    //Integer findHisTeamId(Integer memberId);



}
