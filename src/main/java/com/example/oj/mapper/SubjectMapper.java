package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.connect.SubjectStatusUser;

import com.example.oj.entity.dao.Subject;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 良良
 * @date 2021/12/31 17:23
 */
@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {
    public void updateSubjectStatus(Map<String,Object> datas);

    public SubjectStatusUser selectThis(Map<String,Object> datas);

    public int selectAllById(Integer id);

    public void insertSubjectStatus(Map<String,Object> datas);
}
