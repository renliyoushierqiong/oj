package com.example.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.oj.entity.dao.Action;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 良良
 * @date 2021/12/31 17:21
 */
@Mapper
public interface ActionMapper extends BaseMapper<Action> {
}
