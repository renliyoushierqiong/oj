package com.example.oj.entity.connect;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2022/2/18 16:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_achievement")
public class UserAchievement {
    @TableId(value = "user_id",type = IdType.AUTO)
    Integer id;
    Integer AchievementId;
}
