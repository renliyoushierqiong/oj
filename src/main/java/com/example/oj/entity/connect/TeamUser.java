package com.example.oj.entity.connect;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2022/1/5 23:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "team_user")
public class TeamUser {
    Integer id;
    Integer team_id;
    Integer user_id;
    String user_name;
    boolean captain;
}
