package com.example.oj.entity.connect;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor

@TableName("subject_lists")
public class SubjectLists {
    @TableField(value = "user_id")
    private Integer userid;

    @TableField("user_name")
    private String username;

    @TableField("subject_num")
    private Integer subjectNum;

    @TableField("name")
    private String name;


}
