package com.example.oj.entity.connect;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.sql.In;

import java.time.LocalDateTime;

/**
 * 这个用于查询这个用户是否做了这个题目
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "subject_status")
public class SubjectStatusUser {
    @TableField(exist = false)
    public static int finish=1;
    @TableField(exist = false)
    public static int notFinish=0;

    @TableId(value = "status_id",type = IdType.AUTO)
    private Integer statusId;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "subject_id")
    private Integer subjectId;

    @TableField(value = "status")
    private Integer status;

    @TableField(value = "time")
    private LocalDateTime time;
}
