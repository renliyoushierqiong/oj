package com.example.oj.entity.connect;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.relational.core.sql.In;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/6 17:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MatchUserTeam {
    @TableId(value = "id" ,type = IdType.AUTO)
    private Integer id;
    private Integer match_id;
    private Integer team_id;
    private Integer user_id;
    private String team_name;

}
