package com.example.oj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2021/12/31 18:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamVo {
    private String name;
    private Integer captionId;
    private String describe;
}
