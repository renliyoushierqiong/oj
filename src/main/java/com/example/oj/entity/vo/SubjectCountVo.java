package com.example.oj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author 人生若只如初见
 * @Date 2022/2/17 17:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectCountVo {
    List<SubjectVo> subjectVoList;
    Integer count;
}
