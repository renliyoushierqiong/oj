package com.example.oj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2021/12/31 18:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {
    private String username;
    private String password;
    private String email;
    private String emailCode;
    private String millis;
}
