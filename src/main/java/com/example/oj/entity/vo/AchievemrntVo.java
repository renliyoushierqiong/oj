package com.example.oj.entity.vo;

import com.example.oj.entity.dao.Achievement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author 人生若只如初见
 * @Date 2022/4/17 22:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AchievemrntVo {
    private String name;
    private Integer id;
    private Integer sno;
    private List<Achievement> achievemrntVoList;

}
