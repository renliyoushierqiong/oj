package com.example.oj.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 人生若只如初见
 * @Date 2022/1/25 17:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectVo {
    private Integer subject_id;
    private String subject_name;
    //private String describes;
    private Integer grade;//难度等级
    private Integer status;
    private String time;//预计接替时间
    private Integer solveMan;//解出人数

}
