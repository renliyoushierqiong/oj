package com.example.oj.entity.teamSubject;


//查询团队排行的时候就用这个类去返回(最终返回这个)



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;





@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamSubjectInfo {


    private Integer teamId;
    private String teamName;

    private Integer subNumber;

}
