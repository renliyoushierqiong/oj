package com.example.oj.entity;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @Author 人生若只如初见
 * @Date 2021/12/31 16:11
 */
@Data
public class Resp<T> {
    //响应码
    private int code;
    //响应信息
    private String msg;
    //响应数据
    private T data;

    public Resp(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Resp(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public Resp(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public Resp(HttpStatus status) {
        code = status.value();
        msg = status.name();
        this.data = null;
    }
}