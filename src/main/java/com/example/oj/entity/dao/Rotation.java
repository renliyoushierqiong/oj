package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2022/2/11 23:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "rotation")
public class Rotation {
    @TableId(value = "name")
    String name;
}
