package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author 良良
 * @date 2021/12/31 16:23
 * 功能操作
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "action")
public class Action {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;


}
