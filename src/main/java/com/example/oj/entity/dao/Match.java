package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2021/12/31 16:46
 * 比赛信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "matches")
public class Match {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;//序号
//    @TableField(value = "name")
    private String name;
    @TableField(value = "teacher_name")
    private String teacherName;
    @TableField(value = "describes")
    private String describes;

    private Integer isDelete;
    private Integer numberMan;

    private String signStart;
    private String signEnd;

    private String startTime;
    private String endTime;
    @TableField("m_id")
    private Long mId;//ID

    @TableField("result_status")
    private String resultStatus;

}
