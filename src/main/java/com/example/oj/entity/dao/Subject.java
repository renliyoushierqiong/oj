package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2021/12/31 16:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "subject")
public class Subject {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;

    @TableField(value = "describes")
    private String describes;
    private String answer;

    @TableField(value = "positions")
    private String positions;
    private Integer grade;
    private Integer matchId;
    private String input;
    private String output;
    private String input1;
    private String output1;
    private String time;
}
