package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 良良
 * @date 2021/12/31 16:45
 * 学生提交答案
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "answer")
public class Answer {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer teamId;
    private String subjectID;

    @TableField(value = "positions")
    private String positions;
    private LocalDateTime time;

}
