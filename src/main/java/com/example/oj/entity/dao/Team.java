package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2021/12/31 16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "team")
public class Team {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String name;

    @TableField(value = "captionId")
    private Integer captionId;

    @TableField(value = "describes")
    private String describes;
    @TableField(value = "matchId")
    private Integer matchId;

    private Integer ranking;

    @TableField(value = "is_delete")
    private Integer is_delete;

    @TableField(value = "instructor_name")
    private String instructor_name;

}
