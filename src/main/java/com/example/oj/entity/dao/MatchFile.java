package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author 人生若只如初见
 * @Date 2022/4/21 21:40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("match_file")
public class MatchFile {
    @TableId("match_name")
    private String matchName;
    @TableField("file_name")
    private String fileName;
}
