package com.example.oj.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 良良
 * @date 2022/2/18 15:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "achievement")
public class Achievement {
    @TableId(value = "id",type = IdType.AUTO)
    Integer id;
    String name;
    Integer types;
    Integer userId;

}
