package com.example.oj;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author 良良
 * @date 2022/3/16 21:40
 */
@SpringBootTest
public class redisTest {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void demo(){
        redisTemplate.opsForValue().set("tt","ceshi");
        String tt = (String)redisTemplate.opsForValue().getAndDelete("tt");
//        redisTemplate.delete("tt");

        System.out.println(tt);
    }
}
