package com.example.oj;

import com.example.oj.entity.dao.Rotation;
import com.example.oj.entity.dao.Subject;
import com.example.oj.entity.vo.UserVo;
import com.example.oj.mapper.RotationMapper;
import com.example.oj.service.SubjectService;
import com.example.oj.tool.FileUpload;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 良良
 * @date 2022/1/17 21:40
 */
@SpringBootTest
public class FileTest {

   /* private final SubjectService subjectService;
    @Autowired
    FileTest(SubjectService subjectService){
        this.subjectService=subjectService;
    }

    @Autowired
    RotationMapper rotationMapper;

    @Test
    public void FileDemo() throws IOException {

        XSSFWorkbook workbook=new XSSFWorkbook(new FileInputStream("D://zhuomian/oj.xlsx"));
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row = sheet.getRow(1);
        XSSFCell cell = row.getCell(2);
        List list=new ArrayList();
        switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        list.add(cell.getRichStringCellValue().getString());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            list.add(cell.getDateCellValue());
                        } else {
                            list.add(cell.getNumericCellValue());
                        }
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        list.add(cell.getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        list.add(cell.getCellFormula());
                        break;
                    default:
                        list.add("");
        }
        System.out.println(list);

    }
    @Test
    public void FileDemo1() throws IOException {

        XSSFWorkbook workbook=new XSSFWorkbook(new FileInputStream("D://zhuomian/oj.xlsx"));
        Subject[] subjects = null;

        for (int i=0;i<workbook.getNumberOfSheets();i++){

            Sheet sheet = workbook.getSheetAt(i);
            int number = sheet.getPhysicalNumberOfRows();

           subjects=new Subject[number];
            for (int j=0; j<number;j++){
                subjects[j]=new Subject();
                Row row = sheet.getRow(j+1);
                if(row != null){
                    for (int k=1;k<9;k++){
                        Cell cell = row.getCell(k);

                        if(cell!=null){

                        switch (k){
                            case 1:
                                subjects[j].setName(cell.getRichStringCellValue().getString());
                                break;
                            case 2:
                                subjects[j].setDescribes(cell.getRichStringCellValue().getString());
                                break;
                            case 3:
                                subjects[j].setAnswer(cell.getRichStringCellValue().getString());
                                break;
                            case 4:
                                subjects[j].setPositions(cell.getRichStringCellValue().getString());
                                break;
                            case 5:
                                subjects[j].setGrade(new Double(cell.getNumericCellValue()).intValue());
                                break;
                            case 6:
                                subjects[j].setMatchId(new Double(cell.getNumericCellValue()).intValue());
                                break;
                            case 7:
                                subjects[j].setInput(cell.getRichStringCellValue().getString());
                                break;
                            case 8:
                                subjects[j].setOutput(cell.getRichStringCellValue().getString());
                                break;

                        }
                        }


                    }
                }
            }

        }


    }
    @Test
    public void demo(){
        UserVo[] userVo=new UserVo[2];
        userVo[0]=new UserVo();
        userVo[0].setPassword("111");
        System.out.println(userVo[0]);
    }

    @Test
    public void demo1() {
        Rotation r =new Rotation("上");
        rotationMapper.insert(r);
        //rotationMapper.updateById(r);
    }*/



}
